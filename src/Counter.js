import React from 'react';
import Input from "./Input";
import PropTypes from 'prop-types';
import {withTheme} from "./Theme";
import {connect} from "react-redux";
import {getCounterValue, getList} from "./redux/reducers";
import {setCounter} from "./redux/actions";

class Counter extends React.Component {
    componentDidMount() {
        this.initializeState();
    }

    onStartClick = () => {
        const { operator, id, setCounter, counter } = this.props;

        const newValue = operator(counter);
        setCounter(id, newValue);
    };

    onResetClick = () => {
        this.initializeState();
    };

    initializeState() {
        const { initialValue, id, setCounter } = this.props;
        setCounter(id, initialValue, true);
    }


    isModulo() {
        return this.props.counter % 8 === 0;
    }

    render() {
        const { moduloLabel, theme, counter, list } = this.props;

        return (
            <div style={theme.counter.container}>
                <div style={theme.counter.header}>ReactCounter: {counter}</div>
                {
                    this.isModulo() && moduloLabel
                }
                <div style={theme.counter.btnContainer}>
                    <Input label="Start"  onClick={this.onStartClick} />
                    <Input label="Reset"  onClick={this.onResetClick} />
                </div>
                <div style={theme.counter.listContainer}>
                    {list && list.map((v) => <span key={v}>{v}, </span>)}
                </div>
            </div>
        );
    }
}

Counter.propTypes = {
    id: PropTypes.string.isRequired,
    operator: PropTypes.func.isRequired,
    moduloLabel: PropTypes.node,
    initialValue: PropTypes.number,
    theme: PropTypes.object.isRequired,
    setCounter: PropTypes.func.isRequired,
    counter: PropTypes.number,
    list: PropTypes.array,
};

Counter.defaultProps = {
    initialValue: 0,
};

const mapStateToProps = (state, props) => {
    const counter = getCounterValue(state, props.id);
    const list = getList(state, props.id);

    return {
        counter,
        list,
    }
};

const mapDispatchToProps = {
    setCounter,
};

const connector = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default withTheme(connector(Counter));
