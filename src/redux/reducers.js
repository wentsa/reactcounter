import ActionTypes from "./ActionTypes";
import {combineReducers} from "redux";

const counter = (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.SET_COUNTER: {
            const id = action.payload.id;
            const value = action.payload.value;
            return {
                ...state,
                [id]: value,
            };
        }
        default: {
            return state
        }
    }
};

const list = (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.SET_COUNTER: {
            const id = action.payload.id;
            const value = action.payload.value;
            const isReset = action.payload.isReset;
            return {
                ...state,
                [id]: isReset ? [] : [
                    ...state[id],
                    value,
                ]
            };
        }
        default: {
            return state
        }
    }
};

const reducer = combineReducers({
    counter,
    list,
});

export default reducer;

export const getCounterValue = (state, id) => state.counter[id];
export const getList = (state, id) => state.list[id];
