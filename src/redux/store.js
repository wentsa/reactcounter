import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import counterApp from './reducers'

// Logger with default options
import logger from 'redux-logger';

const store = createStore(counterApp, composeWithDevTools(
    applyMiddleware(logger),
));

export default store;
