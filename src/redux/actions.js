import ActionTypes from "./ActionTypes";

export const setCounter = (id, value, isReset) => ({
    type: ActionTypes.SET_COUNTER,
    payload: {
        id,
        value,
        isReset,
    }
});
