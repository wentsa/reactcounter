import * as React from "react";

const theme = {
    app: {
        textAlign: 'center',
        margin: 100,
        fontFamily: 'sans-serif',
    },
    counter: {
        container: {
            padding: 10,
        },
        header: {
            fontSize: 20,
        },
        btnContainer: {
            padding: 10,
        },
        listContainer: {
            fontStyle: 'italic',
        },
    },
    input: {
        button: {
            backgroundColor: '#4CAF50',
            border: 'none',
            color: 'white',
            padding: '15px 32px',
            textAlign: 'center',
            textDecoration: 'none',
            display: 'inline-block',
            fontSize: 16,
            margin: 3,
            cursor: 'pointer',
        }
    },
};
export const ThemeContext = React.createContext(theme);

export const ThemeProvider = ({children}) => (
    <ThemeContext.Provider value={theme}>
        {children}
    </ThemeContext.Provider>
);

export const withTheme = (WrappedComponent) => {
    return class NewComponent extends React.Component {
        static contextType = ThemeContext;

        render() {
            return <WrappedComponent {...this.props} theme={this.context} />
        }
    };
};

