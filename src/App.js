import React from 'react';
import './App.css';
import Counter from "./Counter";
import {withTheme} from "./Theme";

class App extends React.Component {
    componentDidMount() {
        document.title = "Counter";
    }

    render() {
        return (
            <div style={this.props.theme.app}>
                <Counter
                    operator={(value) => value + 1}
                    moduloLabel="Modulo"
                    initialValue={1}
                    id="plus"
                />
                <Counter
                    operator={(value) => value - 1}
                    moduloLabel={<div style={{ color: 'red' }}>mod</div>}
                    initialValue={-1}
                    id="minus"
                />
                <Counter operator={(value) => value + 2} id="plus2" />
            </div>
        );
    }
}

export default withTheme(App);
