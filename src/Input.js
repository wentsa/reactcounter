import React from "react";
import PropTypes from 'prop-types';
import {withTheme} from "./Theme";

const Input = ({label, onClick, theme}) =>
    <input type="button" value={label} onClick={onClick} style={theme.input.button} />;

Input.propTypes = {
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withTheme(Input);
